#!/usr/bin/bash
#
# SPDX-FileCopyrightText: 2024 David Runge <dvzrv@archlinux.org>
# SPDX-License-Identifier: Apache-2.0 OR MIT
#
# Output the name of a package on stdout, if any of a package's check dependencies can not be found in the list of
# installed packages during build.
#
# Requirements: repod
#
# Run on a list of packages (e.g. `./no_check_pkgs.sh *.pkg.tar.zst`)
#
# WARNING: Output contains false-positives, if check dependencies contain virtual dependencies, as the information on
# installed packages is always fully resolved during build time and therefore may not match a check dependency.

set -euo pipefail

get_checkdepends() {
  pkg="$1"
  repod-file package inspect -P "$pkg" | jq -r '.checkdepends | if . == null then [] else . end | .[]'
}

get_installed() {
  pkg="$1"
  repod-file package inspect -B "$pkg" | jq -r '.installed[]'
}

get_name() {
  pkg="$1"
  repod-file package inspect -P "$pkg" | jq -r '.name'
}

check() {
  local pkg=""

  pkg="$(realpath "$1")"
  local checkdepends=( $(get_checkdepends "$pkg") )
  local installed=( $(get_installed "$pkg") )

  for check_pkg in "${checkdepends[@]}"; do
    local included=0
    for installed_pkg in "${installed[@]}"; do
      if [[ "$installed_pkg" == "$check_pkg"* ]]; then
        included=1
      fi
    done
    if ((!included)); then
      printf "%s\n" "$(get_name "$pkg")"
      break
    fi
  done
}

for pkg in "$@"; do
  check "$pkg"
done

exit 0
