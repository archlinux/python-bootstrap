# python-bootstrap

Bootstrapping for Python projects, that are essential for [PEP 517] based packaging workflows.

## Bootstrapping

When Arch Linux [upgrades the Python interpreter version] to a new major or minor version, it also has to rebuild all Python packages against that new interpreter version.
With [PEP 517] many different build backends exist, that are required for building the entire set of available packages.
However, even the build backends require each other (partially) during build time.
Therefore a *set of initial packages* is bootstrapped using this project - only relying on the Python interpreter and the sources of the packages in question.

---
**NOTE**

Bootstrapping implies that the *set of initial packages* is packaged fully vendored, which is usually not how Arch Linux intends to package those upstreams!
In a second round of rebuilds after bootstrapping, the *set of initial packages* is therefore devendored, before commencing with the rebuild of all Python packages against the new interpreter!

---

## Tracked components

Currently the following upstream projects (the *set of initial packages*) are tracked as git submodule in this project:

- [pypa/build] ([python-build])
- [pypa/flit/flit_core] ([python-flit-core])
- [pypa/installer] ([python-installer])
- [pypa/packaging] ([python-packaging])
- [pypa/pyproject-hooks] ([python-pyproject-hooks])
- [pypa/setuptools] ([python-setuptools])
- [pypa/wheel] ([python-wheel])

This set of projects represents the base set of packages required to rebuild the remaining packages against a new Python interpreter version.

## Usage

### Repository

This repository contains git submodules, which need to be kept in sync with the repository.
After cloning this repository run the following to synchronize all submodules:

```bash
git submodule update --init --recursive
```

### Build wheels

To build the wheel distributables of all [tracked components], run:

```bash
python -m bootstrap.build
```

The built wheels are placed below a top-level `dist/` directory.

### Install wheels

To install a previously built wheel distributable, run:

```bash
python -m bootstrap.install dist/<component>
```

Here `<component>` represents the full wheel file name of the [tracked component].

---
**NOTE**

The `bootstrap.install` module supports the `--destdir`/ `-d` option to provide a specific destination directory to install to.

---

## Releases

The timing, content and format of releases of this project are tied to two considerations:

- the available versions of [tracked components] in the Arch Linux repositories
- the version of the Python interpreter to bootstrap for

The tagging scheme follows the Python interpreter versioning in major and minor version (as those are the relevant indicators for the ecosystem requiring bootstrap).
The patch-level version of the tag is reserved for changes to this project required during bootstrap (e.g. when one of the [tracked components] requires an upgrade).

This means, that if e.g. the Python interpreter version `X.Y` is released

- all submodules for [tracked components] are updated to the versions currently available in the official repositories
- a tag `X.Y.0` is created for this project

If last minute changes are required for this repository, a new tag is created, only increasing the patch-level version (e.g. `X.Y.1`).

## License

This project is licensed under the terms of the [MIT].
All code contributions to this repository - unless noted otherwise - automatically fall under the terms of the aforementioned license.

[PEP 517]: https://peps.python.org/pep-0517/
[upgrades the Python interpreter version]: https://gitlab.archlinux.org/archlinux/packaging/packages/python#bootstrapping-new-python-interpreter-version
[pypa/build]: https://github.com/pypa/build/
[python-build]: https://gitlab.archlinux.org/archlinux/packaging/packages/python-build/
[pypa/flit/flit_core]: https://github.com/pypa/flit/tree/main/flit_core
[python-flit-core]: https://gitlab.archlinux.org/archlinux/packaging/packages/python-flit-core/
[pypa/installer]: https://github.com/pypa/installer/
[python-installer]: https://gitlab.archlinux.org/archlinux/packaging/packages/python-installer/
[pypa/packaging]: https://github.com/pypa/packaging/
[python-packaging]: https://gitlab.archlinux.org/archlinux/packaging/packages/python-packaging/
[pypa/pyproject-hooks]: https://github.com/pypa/pyproject-hooks/
[python-pyproject-hooks]: https://gitlab.archlinux.org/archlinux/packaging/packages/python-pyproject-hooks/
[pypa/setuptools]: https://github.com/pypa/setuptools/
[python-setuptools]: https://gitlab.archlinux.org/archlinux/packaging/packages/python-setuptools/
[pypa/wheel]: https://github.com/pypa/wheel/
[python-wheel]: https://gitlab.archlinux.org/archlinux/packaging/packages/python-wheel/
[tracked components]: #tracked-components
[MIT]: /LICENSE
